var isMobile = /Android|iPhone|iPod|IEMobile/i.test(navigator.userAgent);
var isRetina = window.devicePixelRatio > 1;

function fitSize([width, height], [rectWidth, rectHeight], fill) {
  var ratio = height / width,
      maxRatio = rectHeight / rectWidth,
      scale = (fill
          ? ratio > maxRatio
          : ratio < maxRatio)
              ? rectWidth / width
              : rectHeight / height;
  return [
    width * scale,
    height * scale
  ];
}

export default function getImageSize(windowSize, originalSize, widths) {
  var [ width, height ] = fitSize(windowSize, originalSize, true);

  if (isRetina && !isMobile) {
    width *= 2;
    height *= 2;
  }

  var imageSize = widths[0];
  for (var i = 1, l = widths.length; i < l; i++) {
    var size = widths[i];
    if (size > width && size < imageSize) {
      imageSize = size;
    }
  }
  return imageSize;
}
