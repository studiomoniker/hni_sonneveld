export default function preload(images, callback) {
  var count = images.length;
  var errored = false;
  images.forEach((src) => {
    var img = document.createElement('img');
    img.addEventListener('load', () => {
      count--;
      if (count === 0)
        callback();
    });
    img.addEventListener('error', (err) => {
      if (!errored)
        callback(err);
      errored = true;
    });
    img.src = src;
  });
}