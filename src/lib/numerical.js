// Precision when comparing against 0
// References:
//  http://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html
//  http://www.cs.berkeley.edu/~wkahan/Math128/Cubic.pdf
/**
 * A very small absolute value used to check if a value is very close to
 * zero. The value should be large enough to offset any floating point
 * noise, but small enough to be meaningful in computation in a nominal
 * range (see MACHINE_EPSILON).
 */
export const EPSILON = 1e-12;

const DEGREES = 180 / Math.PI;

export const mix = (value1, value2, ratio) => value1 * (1 - ratio) + value2 * ratio;

export const radiansToDegrees = rad => rad * DEGREES;

export const degreesToRadians = deg => deg / DEGREES;

export const format = number => Math.round(number * 1e5) / 1e5;

export const isZero = number => number < EPSILON;
