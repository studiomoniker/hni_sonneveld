import * as numerical from './numerical';

/**
 * # Point - 2D point utility methods for common vector operations
 */

/**
 * # Manipulation
 */

export const add = (a, b, dest = a) => {
  dest[0] = a[0] + b[0];
  dest[1] = a[1] + b[1];
  return dest;
};

export const addNum = (point, num, dest = point) => {
  dest[0] = point[0] + num;
  dest[1] = point[1] + num;
  return dest;
};

export const addX = (point, number) => {
  point[0] += number;
  return point;
};

export const addY = (point, number) => {
  point[1] += number;
  return point;
};

export const subtract = (a, b, dest = a) => {
  dest[0] = a[0] - b[0];
  dest[1] = a[1] - b[1];
  return dest;
};

export const subtractNum = (point, num, dest = point) => {
  dest[0] = point[0] - num;
  dest[1] = point[1] - num;
  return dest;
};

export const subtractX = (point, number) => {
  point[0] -= number;
  return point;
};

export const subtractY = (point, number) => {
  point[1] -= number;
  return point;
};

export const divide = (a, b, dest = a) => {
  dest[0] = a[0] / b[0];
  dest[1] = a[1] / b[1];
  return dest;
};

export const divideNum = (point, num, dest = point) => {
  dest[0] = point[0] / num;
  dest[1] = point[1] / num;
  return dest;
};

export const divideX = (point, number) => {
  point[0] /= number;
  return point;
};

export const divideY = (point, number) => {
  point[1] /= number;
  return point;
};

export const invert = (point, dest = point) => {
  dest[0] = -point[0];
  dest[1] = -point[1];
  return dest;
};

export const invertX = (point, dest = point) => {
  dest[0] = -point[0];
  return dest;
};

export const invertY = (point, dest = point) => {
  dest[1] = -point[1];
  return dest;
};

export const modulo = (a, b, dest = a) => {
  dest[0] = a[0] % b[0];
  dest[1] = a[1] % b[1];
  return dest;
};

export const moduloNum = (point, number, dest = point) => {
  dest[0] = point[0] % number;
  dest[1] = point[1] % number;
  return dest;
};

export const moduloX = (point, number, dest = point) => {
  dest[0] = point[0] % number;
  return dest;
};

export const moduloY = (point, number, dest = point) => {
  dest[1] = point[1] % number;
  return dest;
};

export const multiply = (a, b, dest = a) => {
  dest[0] = a[0] * b[0];
  dest[1] = a[1] * b[1];
  return dest;
};

export const multiplyNum = (point, num, dest = point) => {
  dest[0] = point[0] * num;
  dest[1] = point[1] * num;
  return dest;
};

export const multiplyX = (point, num, dest = point) => {
  dest[0] = point[0] * num;
  return dest;
};

export const multiplyY = (point, num, dest = point) => {
  dest[1] = point[1] * num;
  return dest;
};

export const normalize = (point, dest = point) => (multiplyNum(
  point,
  1 / length(point),
  dest
));

export const round = (point, dest = point) => {
  dest[0] = Math.round(point[0]);
  dest[1] = Math.round(point[1]);
  return dest;
};

export const abs = (point, dest = point) => {
  dest[0] = Math.abs(point[0]);
  dest[1] = Math.abs(point[1]);
  return dest;
};

export const ceil = (point, dest = point) => {
  dest[0] = Math.ceil(point[0]);
  dest[1] = Math.ceil(point[1]);
  return dest;
};

export const floor = (point, dest = point) => {
  dest[0] = Math.floor(point[0]);
  dest[1] = Math.floor(point[1]);
  return dest;
};

export const mix = (a, b, amount, dest = a) => {
  dest[0] = numerical.mix(a[0], b[0], amount);
  dest[1] = numerical.mix(a[1], b[1], amount);
  return dest;
};

export const clone = ([x, y]) => [x, y];

export const set = (a, b) => {
  a[0] = b[0];
  a[1] = b[1];
  return a;
};

export const vector = (a, b) => subtract(a, b, [0.0, 0.0]);

export const zero = (point) => {
  point[0] = 0;
  point[1] = 0;
  return point;
};

export const dot = ([x, y]) => x * x + y * y;

export const cross = (a, b) => (a[0] * b[1]) - (a[1] * b[0]);

export const project = (vector, dest = vector) => {
  const coeff = isZero(vector)
    ? 0
    : dot(vector) / lengthSquared(vector);
  dest[0] = coeff * vector[0];
  dest[1] = coeff * vector[1];
  return dest;
};

const angle = point => numerical.radiansToDegrees(angleInRadians(point));

const angleInRadians = (point, toPoint) => {
  if (!toPoint) {
    return isZero(point)
      ? 0
      : Math.atan2(point[1], point[0]);
  }
  const div = length(point) * length(toPoint);
  if (numerical.isZero(div)) {
    return NaN;
  }
  const a = dot(point, toPoint) / div;
  return Math.acos(
    a < -1
      ? -1
      : a > 1
        ? 1
        : a
  );
};

export const directedAngle = (a, b) => Math.atan2(cross(a, b), dot(a, b)) * 180 / Math.PI;

export const setAngle = (point, angle) => setAngleInRadians(numerical.degreesToRadians(angle));

const setAngleInRadians = (point, angle, dest = point) => {
  if (!isZero(point)) {
    const pointLength = length(point);
    dest[0] = Math.cos(angle) * pointLength;
    dest[1] = Math.sin(angle) * pointLength;
  }
  return dest;
};

export const rotateThrough = (point, angle, center, dest = point) => {
  if (angle === 0) return dest;
  const radians = numerical.degreesToRadians(angle);
  const s = Math.sin(radians);
  const c = Math.cos(radians);
  let [x, y] = point;
  if (center) {
    x -= center[0];
    y -= center[1];
  }
  let x1 = x * c - y * s;
  let y1 = x * s + y * c;
  if (center) {
    x1 += center[0];
    y1 += center[1];
  }
  dest[0] = x1;
  dest[1] = y1;
  return dest;
};

export const rotate = (point, angle, dest) => rotateBy(point, angle, null, dest);

export const rotateBy = (point, rotation, dest = point) => rotate(
  point,
  angle(point) + rotation, dest
);

export const distance = (a, b) => Math.sqrt(distanceSquared(a, b));

export const distanceSquared = ([x1, y1], [x2, y2]) => {
  const dx = x2 - x1;
  const dy = y2 - y1;
  return dx * dx + dy * dy;
};

export const length = (point) => Math.sqrt(lengthSquared(point));

// double-dog-leg hypothenuse approximation
// http://forums.parallax.com/discussion/147522/dog-leg-hypotenuse-approximation
export const approximateLength = ([x, y]) => {
  const absX = Math.abs(x);
  const absY = Math.abs(y);
  const lo = Math.min(absX, absY);
  const hi = Math.max(absX, absY);
  return hi + 3 * lo / 32 + Math.max(0, 2 * lo - hi) / 8 + Math.max(0, 4 * lo - hi) / 16;
};

export const lengthSquared = ([x, y]) => x * x + y * y;

export const setLength = (point, pointLength, dest = point) => {
  if (isZero(point)) {
    const pointAngle = angle(point);
    dest[0] = Math.cos(pointAngle) * length;
    dest[1] = Math.sin(pointAngle) * length;
  } else {
    const scale = pointLength / length(point);
    dest[0] = point[0] * scale;
    dest[1] = point[1] * scale;
  }
  return dest;
};

export const limitLength = (point, length, dest = point) => {
  const len = length(point);
  if (len > length) {
    const scale = length / len;
    dest[0] = point[0] * scale;
    dest[1] = point[1] * scale;
  }
  return dest;
};

export const isClose = (a, b, tolerance) => distanceSquared(a, b) < (tolerance * tolerance);

export const isCollinear = (a, b) => numerical.isZero(Math.abs(cross(a, b)));

export const isOrthogonal = (a, b) => numerical.isZero(Math.abs(dot(a, b)));

export const isZero = ([x, y]) => numerical.isZero(x) && numerical.isZero(y);

export const equal = (a, b) => a[0] === b[0] && a[1] === b[1];

/**
 * # Utility Methods
 */

export const toObject = ([x, y]) => ({ x, y });

export const fromObject = ({ x, y }) => [x, y];

export const fromAngleWithLength = (angle, length) => setAngle([length, 0], angle);

export const randomVector = (len = 1.0) => {
  const r = Math.random() * 2.0 * Math.PI;
  return [
    Math.cos(r) * len,
    Math.sin(r) * len,
  ];
};

export const random = () => ([Math.random(), Math.random()]);

export const min = ([x1, y1], [x2, y2], dest = [0.0, 0.0]) => {
  dest[0] = Math.min(x1, x2);
  dest[1] = Math.min(y1, y2);
  return dest;
};

export const max = ([x1, y1], [x2, y2], dest = [0.0, 0.0]) => {
  dest[0] = Math.max(x1, x2);
  dest[1] = Math.max(y1, y2);
  return dest;
};
