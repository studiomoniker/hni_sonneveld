import 'points';
import { voronoi } from 'd3-voronoi';

import * as Point from './lib/point';
import loop from './lib/rafLoop';
import message from './lib/message';
import getImageSize from './imageSize';
import preload from './preloadImages';
import { imageWidths, originalSize } from '../settings.json';

let pointMouse = [0.0, 0.0];
const pointFixed = [0.0, 0.0];
const pointMouseAnimated = [0.0, 0.0];
const pointFixedAnimated = [0.0, 0.0];

let windowSize = getWindowSize();
const clipPolygon = document.querySelector('#clip-polygon');
const width = getImageSize(originalSize, windowSize, imageWidths);
const srcs = [`assets/images/01-${width}.jpg`, `assets/images/02-${width}.jpg`];
onWindowResize();
window.addEventListener('resize', onWindowResize);
window.addEventListener('pointermove', (event) => {
  pointMouse = [event.clientX, event.clientY];
});

loop.add(() => {
  // Update the positions of the points,
  // and if they changed, update the mask:
  if (easePoints(pointMouseAnimated, pointMouse) || easePoints(pointFixedAnimated, pointFixed)) {
    const points = voronoi()
      .extent([[0, 0], windowSize])
      .polygons([pointMouseAnimated, pointFixedAnimated])[0];
    if (points) {
      clipPolygon.setAttribute(
        'points',
        points.map(([x, y]) => `${x} ${y}`).join(', ')
      );
    }
  }
});

// Preload the images:
preload(srcs, (err) => {
  if (err) throw err;
  // Display the images:
  const domImages = document.querySelectorAll('image');
  srcs.forEach((src, index) => {
    displayImage(domImages[index], src);
  });
});

function displayImage(el, src) {
  el.setAttributeNS('http://www.w3.org/1999/xlink', 'href', src);
  el.setAttribute('class', 'loaded');
  // Wait till the image is shown, then remove the black path:
  setTimeout(() => {
    loop.once(() => {
      const prevNode = el.previousElementSibling;
      if (prevNode) {
        el.parentNode.removeChild(prevNode);
      }
    });
  }, 1000);
}

function easePoints(a, b, easing = 0.16) {
  const farEnough = Point.distance(a, b) > 1;
  if (farEnough) {
    Point.mix(a, b, easing);
  }
  return farEnough;
}

function getWindowSize() {
  return [window.innerWidth, window.innerHeight];
}

function onWindowResize() {
  windowSize = getWindowSize();
  if (!pointMouse) {
    Point.multiplyNum(windowSize, 0.2, pointMouse);
    Point.multiplyNum(windowSize, 0.2, pointMouseAnimated);
    Point.multiplyNum(windowSize, 0.4, pointFixedAnimated);
  }
  Point.multiplyNum(windowSize, 0.55, pointFixed);
}
